import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CommonService, Todo } from 'src/app/service/common.service';

@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  todo_sub: Subscription;
  form: FormGroup;
  todos: Todo[];


  constructor(
    private fb: FormBuilder,
    private common: CommonService,
    ) {
    this.form = this.fb.group({
      name: new FormControl({value: null, disabled: false}, Validators.required),
      done: new FormControl({value: null, disabled: false}),
    })
  }

  ngOnInit(): void {
    this.todo_sub = this.common._todoList.subscribe((todoList: Todo[]) => {
      this.todos = todoList;
      console.log("Todos: ", this.todos);
      
    })
  }

  addTask() {
    if (!this.form.valid) return false;
    this.common._addTodo(this.form.value);
  }


  removeTask(todo: Todo) {
    this.common._removeTodo(todo);
  }

  changeStatus(todo: Todo) {
    this.common.changeStatus(todo);
  }

}
