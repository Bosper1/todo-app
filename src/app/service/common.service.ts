import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Todo {
  name: string;
  done: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  todoList: Todo[];
  public _todoList: BehaviorSubject<Todo[]> = new BehaviorSubject<Todo[]>([]);

  constructor() {
    this.todoList = [];
  }

  public _addTodo(_todo: Todo) {
    const todo: Todo = _todo;
    this.todoList.push(_todo);
    this._todoList.next(this.todoList)
  }

  public _removeTodo(_todo: Todo) {

    this.todoList.splice(this.todoList.indexOf(_todo), 1);
    this._todoList.next(this.todoList);
  }

  public changeStatus(todo: Todo) {
    const _id = this.todoList.indexOf(todo);
    
    this.todoList[_id].done = !this.todoList[_id].done;
    this._todoList.next(this.todoList);
    
  }

  
}
